package com.example.aimweatherapp.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by david on 27/12/13.
 */
public class DBBasics extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;

    private static final String SQL_CREATE_STATEMENT_POST =
            "CREATE TABLE " + DBContract.DBColumns.TABLE_NAME + " (" +
                    DBContract.DBColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    DBContract.DBColumns.COLUMN_NAME_CITY+ " TEXT, " +
                    DBContract.DBColumns.COLUMN_NAME_COUNTRY + " TEXT," +
                    DBContract.DBColumns.COLUMN_NAME_COUNTY + " TEXT," +
                    DBContract.DBColumns.COLUMN_NAME_LATITUDE + " DOUBLE," +
                    DBContract.DBColumns.COLUMN_NAME_LONGITUDE + " DOUBLE);";

    private static final String SQL_DROP_TABLE_POST =
            "DROP TABLE IF EXISTS " + DBContract.DBColumns.TABLE_NAME + ";";

    public DBBasics(Context context) {
        super(context, DBContract.DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_STATEMENT_POST);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DROP_TABLE_POST);
    }
}
