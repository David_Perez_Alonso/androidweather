package com.example.aimweatherapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.media.Image;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.aimweatherapp.Utils.iconsManagement;
import com.example.aimweatherapp.controls.CustomScrollView;
import com.example.aimweatherapp.model.day;
import com.example.aimweatherapp.model.forecast;
import com.example.aimweatherapp.model.location;
import com.example.aimweatherapp.model.locationList;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Calendar;
import java.util.zip.Inflater;

/**
 * Created by david on 2/01/14.
 */
public class ForecastAdapter extends PagerAdapter {

    Context ctx;
    locationList locations;
    HorizontalScrollView horizontalScrollView;


    public ForecastAdapter(Context ctx, locationList loc)
    {
        this.ctx = ctx;
        this.locations = loc;
    }

    @Override
    public int getCount() {
        return locations.get(0).getDays().size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        LayoutInflater inflater = (LayoutInflater) this.ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View layout = inflater.inflate(R.layout.day_item,null);


        HorizontalScrollView horizontalScrollView = (HorizontalScrollView) layout.findViewById(R.id.hoursScrollView);
        LinearLayout lHours = (LinearLayout) layout.findViewById(R.id.linearHours);
        //LinearLayout lIcons = (LinearLayout) layout.findViewById(R.id.al);


        int i = 0;
        double avgHumidity = 0;
        int avgPrecip = 0;
        int avgPressure= 0;
        int avgWind = 0;
        for(forecast f : locations.getLocations().get(0).getDays().get(position).getForecasts())
        {
            LinearLayout rl = new LinearLayout(layout.getContext());
            rl.setMinimumWidth(100);
            rl.setMinimumHeight(110);
            rl.setOrientation(LinearLayout.VERTICAL);

            TextView tx = new TextView(layout.getContext());
            /*tx.setWidth(100);
            tx.setHeight(50);*/
            tx.setPadding(40,10,10,10);
            //tx.setTextAlignment(TextView.TEXT_ALIGNMENT_CENTER);

            String Texto = Integer.toString(i);
            if(Texto.length() == 1)
                Texto = "0" + Texto;
            tx.setText(Texto);

            rl.addView(tx);

            ImageView iv = new ImageView(layout.getContext());

            iv.setImageBitmap(iconsManagement.get(f, ctx));
            rl.addView(iv);

            TextView txTemp = new TextView(layout.getContext());
            txTemp.setPadding(40,10,10,0);
            txTemp.setText(f.getDegrees().toString() + "º");

            rl.addView(txTemp);

            i+= 3;

            lHours.addView(rl);

            avgHumidity = avgHumidity + f.getHumidity();
            avgPrecip = avgPrecip + f.getPrecipMM();
            avgPressure = avgPressure + f.getPressure();
            avgWind = avgWind + f.getWind_speedKmph();

            if (i == 24)
            {
                avgHumidity = avgHumidity / 7;
                avgPrecip = avgPrecip / 7;
                avgPressure = avgPressure / 7;
                avgWind = avgWind / 7;
            }

        }

        BigDecimal big = new BigDecimal(avgHumidity);
        big = big.setScale(2, RoundingMode.HALF_UP);

        TextView txtHumity = (TextView) layout.findViewById(R.id.txtHumidity);
        txtHumity.setText(big + "%");

        TextView txtPrecipMM = (TextView) layout.findViewById(R.id.txtprecipMM);
        txtPrecipMM.setText(Integer.toString(avgPrecip) + "mm");

        TextView txtPressure = (TextView) layout.findViewById(R.id.txtPressure);
        txtPressure.setText(Integer.toString(avgPressure));

        TextView txtWind = (TextView) layout.findViewById(R.id.txtwind);
        txtWind.setText(Integer.toString(avgWind) + "Kh");

        TextView txtHoy = (TextView) layout.findViewById(R.id.txtHoy);

        Calendar calendar = Calendar.getInstance();

        switch (locations.getLocations().get(0).getDays().get(position).getDate().getDay())
        {
            case(0):
                txtHoy.setText(R.string.sunday);
                break;
            case(1):
                txtHoy.setText(R.string.monday);
                break;
            case(2):
                txtHoy.setText(R.string.tuesday);
                break;
            case(3):
                txtHoy.setText(R.string.wendsday);
                break;
            case(4):
                txtHoy.setText(R.string.thursday);
                break;
            case(5):
                txtHoy.setText(R.string.friday);
                break;
            case(6):
                txtHoy.setText(R.string.saturday);
                break;

        }

        String dia = Integer.toString(locations.getLocations().get(0).getDays().get(position).getDate().getDay());
        String mes = Integer.toString(locations.getLocations().get(0).getDays().get(position).getDate().getMonth());

        if (position == 0)
            txtHoy.setText(ctx.getText(R.string.today) + " - " + txtHoy.getText());
        else
            txtHoy.setText(txtHoy.getText());// + " - " + locations.getLocations().get(0).getDays().get(position).getDate().getDay() + "/" +locations.getLocations().get(0).getDays().get(position).getDate().getMonth() );

        ((ViewPager) container).addView(layout, 0);
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(View view, Object o) {

        return view == o;
    }
}
