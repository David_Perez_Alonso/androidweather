package com.example.aimweatherapp.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by root on 15/12/13.
 */
public class day {
    private Date date;
    private int temperature;
    private int maxTemperature;

    private int minTemperature;
    ArrayList<forecast> forecasts;

    public day(JSONObject dayJSON) throws JSONException, ParseException {
        if (dayJSON.has("date"))
        {
            String sDate = dayJSON.getString("date");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date dateDate = sdf.parse(sDate);
            this.date = dateDate;
        }

        this.forecasts = new ArrayList<forecast>();
        JSONArray entries = dayJSON.getJSONArray("hourly");

        for (int i = 0; i< entries.length(); i++) {
            JSONObject hourJSON = entries.getJSONObject(i);
            this.forecasts.add(new forecast(hourJSON));
            //hour h = new hour(i,hourJSON);

        }
    }

    public int getCount()
    {
        return forecasts.size();
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public ArrayList<forecast> getForecasts() {
        return forecasts;
    }

    public void setForecasts(ArrayList<forecast> forecast) {
        this.forecasts = forecast;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public int getMinTemperature() {
        return minTemperature;
    }

    public void setMinTemperature(int minTemperature) {
        this.minTemperature = minTemperature;
    }

    public int getMaxTemperature() {
        return maxTemperature;
    }

    public void setMaxTemperature(int maxTemperature) {
        this.maxTemperature = maxTemperature;
    }
}