package com.example.aimweatherapp.Utils;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.HorizontalScrollView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.aimweatherapp.ForecastAdapter;
import com.example.aimweatherapp.LocationAdapter;
import com.example.aimweatherapp.LocationAdapterSearch;
import com.example.aimweatherapp.MainActivity;
import com.example.aimweatherapp.R;
import com.example.aimweatherapp.model.location;
import com.example.aimweatherapp.model.locationList;
import com.example.aimweatherapp.searchCity;

import java.util.Calendar;

/**
 * Created by david on 26/12/13.
 */

public class JsonHelper
{

    public interface OnTaskCompleted{
        void onTaskCompleted(locationList loc);
    }

    public JsonHelper()
    {}

    private searchCity.OnSearchCompleted pListener;
    public JsonHelper(searchCity.OnSearchCompleted listener)
    {
        this.pListener = listener;
    }

    public void loadCities(String city, Activity _ctx)
    {
        final Activity ctx = _ctx;
        OnTaskCompleted listener = new OnTaskCompleted() {
            @Override
            public void onTaskCompleted(locationList loc) {
                LocationAdapterSearch adapter = new LocationAdapterSearch(ctx, loc);
                ListView list = (ListView) ctx.findViewById(R.id.listCity);
                list.setAdapter(adapter);
                pListener.OnSearchCompleted(loc);
            }
        };

        JsonHelperAsync async = new JsonHelperAsync(listener);
        async.execute("http://api.worldweatheronline.com/free/v1/search.ashx?key=4gsacyj2bjpcjdehp4s8zxgp&query=" + city + "&num_of_results=20&format=json","CITY");
    }


    public void loadForecast(location locloc, View _ctx)
    {
        final View ctx = _ctx;
        Integer i = 0;

        OnTaskCompleted listener = new OnTaskCompleted() {

            @Override
            public void onTaskCompleted(locationList loc) {


                TextView txTemp = (TextView)ctx.findViewById(R.id.temperature);
                txTemp.setText(Integer.toString(loc.get(0).getDays().get(0).getTemperature()) + "º");

                ctx.setBackgroundResource(backgroundsManagement.get(loc.get(0).getDays().get(0).getForecasts().get(0),ctx.getContext()));
                //ctx.setBackground(backgroundsManagement.get(loc.get(0).getDays().get(0).getForecasts().get(0),ctx.getContext()));

                  ForecastAdapter fAdapter = new ForecastAdapter(ctx.getContext(), loc);
                  android.support.v4.view.ViewPager viewPager = (android.support.v4.view.ViewPager) ctx.findViewById(R.id.forecast_view_pager);
                  viewPager.setAdapter(fAdapter);

                ProgressBar mProgress = (ProgressBar) ctx.findViewById(R.id.progress);
                mProgress.setVisibility(View.GONE);

            }
        };


          JsonHelperAsync async = new JsonHelperAsync(listener);
          String coordinates = locloc.getLatitude() + "," + locloc.getLongitude();
          async.execute("http://api.worldweatheronline.com/premium/v1/weather.ashx?key=44mbcse7c49q7jnjfe4k6tse&q=" + coordinates + "&num_of_days=7&tp=3&format=json","FORECAST");
    }


private class JsonHelperAsync extends AsyncTask<Object, Void, locationList> {



    private OnTaskCompleted listener ;

    public JsonHelperAsync(OnTaskCompleted listener)
    {
        this.listener = listener;
    }

    @Override
    protected locationList doInBackground(Object... params) {
        HttpUtils http = new HttpUtils((String)params[0]);
        try
        {
            if ((String)params[1] == "CITY")
            {
              http.connect();
              locationList location = new locationList(http.getJSONObject(),"CITY", 0);
              return location;
            }

            if ((String)params[1] == "FORECAST")
            {
              //locationList list = (locationList)params[2];
              locationList result = new locationList();
              http.connect();
              int i = 0;
              locationList loc = new locationList(http.getJSONObject(), "FORECAST", i);
              result.add(loc.get(0));
              return result;
            }

        }
        catch(Exception ex)
        {
            return new locationList();
        }
        finally {
            http.close();
        }

        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(locationList result) {
        super.onPostExecute(result);
        if (listener != null)
        {

            listener.onTaskCompleted(result);
        }
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }
}
}
