package com.example.aimweatherapp.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;

/**
 * Created by root on 15/12/13.
 */
public class hour {
    private int hour_number;
    ArrayList<forecast> forecasts;


    public hour(int hour, JSONObject hourJSON) throws JSONException, ParseException {
        forecasts.add(new forecast(hourJSON));
    }

    public int getHour_number() {
        return hour_number;
    }

    public void setHour_number(int hour_number) {
        this.hour_number = hour_number;
    }
}
