package com.example.aimweatherapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.TreeMap;

/**
 * Created by david on 17/12/13.
 */
public class locationList implements Parcelable {

    private ArrayList<location> locations;
    private TreeMap<Long, location> byId;


    public static final Creator<locationList> CREATOR = new Creator<locationList>() {
        @Override
        public locationList createFromParcel(Parcel source) {
            locationList retorno = new locationList();
            Parcelable[] locs = source.readParcelableArray(locationList.class.getClassLoader());

            if (locs != null)
            {
                for (Parcelable lc : locs)
                {retorno.add((location) lc);}
            }
            return retorno;
        }

        @Override
        public locationList[] newArray(int size) {
            return new locationList[size];
        }
    };

    public locationList(JSONObject jsonWeather, String type, int i) throws JSONException, ParseException {
        this();

        if (type == "CITY")
        {
          JSONObject feed = jsonWeather.getJSONObject("search_api");
          JSONArray entries = feed.getJSONArray("result");

          for (int j = 0; j< entries.length(); j++) {
            JSONObject loc = entries.getJSONObject(j);
            location locObject = new location(loc,type, 0);
            this.add(locObject);
          }
        }

        if (type == "FORECAST")
        {
            JSONObject feed = jsonWeather.getJSONObject("data");

            location locObject = new location(feed, type, i);
            this.add(locObject);

            //JSONArray entries = feed.getJSONArray("current_condition");

            /*for (int i = 0; i< entries.length(); i++) {
                JSONObject loc = entries.getJSONObject(i);
                location locObject = new location(loc,type);
                this.add(locObject);
            }*/
        }
    }

    public locationList() {
        locations = new ArrayList<location>();
        byId = new TreeMap<Long, location>();
    }

    public void add(location loc) {
        add(new location[]{loc});
    }

    public void add(location... locationArray) {
        this.locations.addAll( Arrays.asList(locationArray) );

        for (location loc : locationArray) {
            this.byId.put((long)this.locations.size()-1, loc);
        }
    }

    public ArrayList<location> getLocations() {
        return locations;
    }

    public TreeMap<Long, location> getById() {
        return byId;
    }

    public location get(long id) {
        return byId.get(id);
    }

    public int getPosition(long id) {
        location loc = get( id );
        return locations.indexOf( loc );
    }

    public int getCount()
    {
        return locations.size();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        location[] locationArray = this.locations.toArray(new location[0]);
        dest.writeParcelableArray(locationArray,0);
    }
}
