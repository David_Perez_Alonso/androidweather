package com.example.aimweatherapp;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.TextView;

import com.example.aimweatherapp.Utils.DBHelper;
import com.example.aimweatherapp.Utils.JsonHelper;
import com.example.aimweatherapp.model.location;
import com.example.aimweatherapp.model.locationList;

public class MainActivity extends ActionBarActivity {

    static locationList locations = new locationList();
    static android.support.v4.view.ViewPager mPager;
    static LocationAdapter mAdapter;

    public interface OnTaskCompleted{
        void onTaskCompleted(boolean bool);
    }

    public interface OnAddedNew{
        void onAddedNew();
    }

    static OnAddedNew listener;

    public MainActivity()
    {

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        listener = new OnAddedNew() {
            @Override
            public void onAddedNew() {
                mAdapter.notifyDataSetChanged();
                mPager.setAdapter(mAdapter);
            }
        };

        locations = new locationList();
        setContentView(R.layout.activity_main);

        mPager = (android.support.v4.view.ViewPager) findViewById(R.id.cities_view_pager);
        //this.getActionBar().setDisplayShowHomeEnabled(false);
        if (android.os.Build.VERSION.SDK_INT <= 10)
        {
            mPager.setPadding(0,0,0,0);
        }

        locations = new locationList();
        getAllCities getCities = new getAllCities();
        getCities.execute();

        //getAllCities getCities = new getAllCities();
        //getCities.execute();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.


        switch (item.getItemId()) {
            case R.id.deleteItem:
                if (locations.getCount() > 0)
                {
                OnTaskCompleted listener = new OnTaskCompleted() {

                    @Override
                    public void onTaskCompleted(boolean bool) {

                        if(bool)
                        {
                          int i = mPager.getCurrentItem();
                          locations.getLocations().remove(i);
                          locations.getById().remove((long)i);
                          mAdapter.notifyDataSetChanged();
                          mPager.setAdapter(mAdapter);
                        }
                    }
                };
                delete async = new delete(listener);
                location loc = locations.getLocations().get((mPager.getCurrentItem()));

                async.execute(loc.getId());
                }
                return true;

            case R.id.addCity:
                Intent intent = new Intent(this, searchCity.class);
                startActivity(intent);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }


        private class getAllCities extends AsyncTask<Void, Void, Cursor>
        {

            @Override
            protected Cursor doInBackground(Void... params) {


                DBHelper dbHelper = new DBHelper(MainActivity.this);
                try
                {
                Cursor result = dbHelper.selectAll();

                if (result.moveToFirst()) {
                    do {
                        location loc = new location(result);
                        MainActivity.locations.add(loc);

                    } while (result.moveToNext());
                }
                return result;
                }
                catch(Exception ex)
                {
                    return null;
                }
                finally {
                    dbHelper.close();
                }
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected void onPostExecute(Cursor cursor) {
                super.onPostExecute(cursor);
                mAdapter = new LocationAdapter(getSupportFragmentManager(),locations);
                mPager.setAdapter(mAdapter);
                //mAdapter.notifyDataSetChanged();
            }

            @Override
            protected void onProgressUpdate(Void... values) {
                super.onProgressUpdate(values);
            }
        }

    private class delete extends AsyncTask<Long, Void, Boolean>
    {

        private OnTaskCompleted listener ;

        public delete(OnTaskCompleted listener)
        {
            this.listener = listener;
        }

        @Override
        protected Boolean doInBackground(Long... params) {


            DBHelper dbHelper = new DBHelper(MainActivity.this);
            try
            {
            dbHelper.delete((long) params[0]);

            return true;
            }
            catch(Exception ex)
            {
                return false;
            }
            finally {
                dbHelper.close();
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            listener.onTaskCompleted(aBoolean);
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }
    }

}
