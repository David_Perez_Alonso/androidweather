package com.example.aimweatherapp.model;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v7.appcompat.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by root on 15/12/13.
 */

public class location implements Parcelable {

    //Properties
    private long id;
    private String name;
    private String country;
    private String county;
    private String city;
    private double latitude = 0;
    private double longitude = 0;

    private int temp = 0;
    private ArrayList<day> days;

    public static final Creator<location> CREATOR = new Creator<location>() {

        @Override
        public location createFromParcel(Parcel source) {
            long id = source.readLong();
            String name = source.readString();
            String country = source.readString();
            String county = source.readString();
            String city = source.readString();
            double latitude = source.readDouble();
            double longitude = source.readDouble();
            int temp = source.readInt();

            return new location(id, name, country, county, city, latitude, longitude);
        }


        @Override
        public location[] newArray(int size) {
            return new location[size];
        }
    };



    public location(long id, String name, String country, String county, String city, double latitude, double longitude) {
        this.id = id;
        this.name = name;
        this.country = country;
        this.county = county;
        this.city = city;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public location(JSONObject loc, String type, int i) throws JSONException, ParseException {

        if (type == "CITY")
        {
          try {
            String city = "";
            String country = "";
            String county = "";
            Double latitude = 0.0;
            Double longitude = 0.0;
            int temp = 0;
          if (loc.has("areaName")){
            city = loc.getJSONArray("areaName").optJSONObject(0).getString("value");}

          if (loc.has("country")){
            country = loc.getJSONArray("country").optJSONObject(0).getString("value");}

          if (loc.has("region")){
            county = loc.getJSONArray("region").optJSONObject(0).getString("value");}

          if (loc.has("latitude")){
            latitude = Double.parseDouble(loc.getString("latitude"));}

          if (loc.has("longitude")){
            longitude = Double.parseDouble(loc.getString("longitude"));}

          this.city = city;
          this.country = country;
          this.county = " - " +  county;
          this.latitude = latitude;
          this.longitude = longitude;

        }
        catch(Exception ex)
        {
            this.city = null;
            this.country = null;
            this.county = null;
            this.latitude = 0;
            this.longitude = 0;
        }
      }

        if (type == "FORECAST")
        {
            try {

                int temp = 0;
                //VAMOS A TRAER PRIMERO EL TIEMPO LOCAL
                days = new ArrayList<day>();

                JSONArray entries = loc.getJSONArray("weather");

                for (int j = 0; j< entries.length(); j++) {
                  JSONObject dayJSON = entries.getJSONObject(j);
                  this.days.add(new day(dayJSON));

                    if (j == 0)
                    {
                        JSONArray entries2 = loc.getJSONArray("current_condition");
                        JSONObject tempJSON = entries2.getJSONObject(0);
                        temp = tempJSON.getInt("temp_C");
                        this.days.get(0).setTemperature(temp);
                    }
                }
            }
            catch(Exception ex)
            {
                this.city = null;
                this.country = null;
                this.county = null;
                this.latitude = 0;
                this.longitude = 0;
                this.temp = 0;
            }
        }
    }

    public location(Cursor cursor)
    {
        this(0, null, null, null,null, 0, 0);
        int id = cursor.getInt(0);
        this.id = id;
        this.city = cursor.getString(1);
        this.country = cursor.getString(2);
        this.county = cursor.getString(3);
        this.latitude = cursor.getDouble(4);
        this.longitude = cursor.getDouble(5);
    }

    public long getId() {
        return id;
    }


    public String getCountry() {
        return country;
    }


    public String getCounty() {
        return county;
    }


    public String getCity() {
        return city;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public ArrayList<day> getDays() {
        return days;
    }

    public void setDays(ArrayList<day> days) {
        this.days = days;
    }

    public int getTemp() {
        return temp;
    }

    public void setTemp(int temp) {
        this.temp = temp;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.name);
        dest.writeString(this.country);
        dest.writeString(this.county);
        dest.writeString(this.city);
        dest.writeDouble(this.latitude);
        dest.writeDouble(this.longitude);
        dest.writeInt(this.temp);
    }
}