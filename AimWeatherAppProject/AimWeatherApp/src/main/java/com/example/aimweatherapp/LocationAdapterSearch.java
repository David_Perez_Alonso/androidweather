package com.example.aimweatherapp;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CursorAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.TwoLineListItem;

import com.example.aimweatherapp.model.location;
import com.example.aimweatherapp.model.locationList;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created by david on 17/12/13.
 */
public class LocationAdapterSearch extends BaseAdapter{

    static class ViewHolder
    {
        TextView txtCity;
        TextView txtCountry;
        TextView txtCounty;
    }

    private Activity ctx;
    private locationList locations;
    private LayoutInflater inflater = null;


    public LocationAdapterSearch(Activity context, locationList locs) {
        this.ctx = context;
        this.locations = locs;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return locations.getCount();
    }

    @Override
    public Object getItem(int position) {
        return locations.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null)
        {
            convertView = inflater.inflate(R.layout.city_view, null);
            holder = new ViewHolder();
            holder.txtCity = (TextView) convertView.findViewById(R.id.txtCity);
            holder.txtCountry = (TextView) convertView.findViewById(R.id.txtCountry);
            holder.txtCounty = (TextView) convertView.findViewById(R.id.txtCounty);
            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.txtCity.setText(locations.get(position).getCity());
        holder.txtCountry.setText(locations.get(position).getCountry());
        holder.txtCounty.setText(locations.get(position).getCounty());

        return convertView;
    }
}
