package com.example.aimweatherapp.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;

/**
 * Created by root on 15/12/13.
 */
public class forecast {
    Integer degrees = 0;
    Integer wind_speedKmph = 0;
    Integer wind_dirDegree = 0;
    Integer precipMM = 0;
    Double humidity = 0.0;
    Integer visibility = 0;
    Integer pressure = 0;
    Integer cloud_cover = 0;
    Integer chance_of_rain = 0;
    Integer chance_of_windy = 0;
    Integer chance_of_sunny = 0;
    Integer chance_of_snow = 0;
    Integer chance_of_fog = 0;
    Integer chance_of_thunder = 0;
    String weather_icon = "";
    String weather_background = "";


    public forecast(JSONObject fJSON)  throws JSONException, ParseException {
         if (fJSON.has("tempC")){
            degrees = fJSON.getInt("tempC");}

         if (fJSON.has("windspeedKmph")){
            wind_speedKmph = fJSON.getInt("windspeedKmph");}

         if (fJSON.has("winddirDegree")){
            wind_dirDegree = fJSON.getInt("winddirDegree");}

         if (fJSON.has("precipMM")){
            precipMM = fJSON.getInt("precipMM");}

         if (fJSON.has("humidity")){
            humidity = fJSON.getDouble("humidity");}

         if (fJSON.has("visibility")){
            visibility = fJSON.getInt("visibility");}

         if (fJSON.has("pressure")){
            pressure = fJSON.getInt("pressure");}

         if (fJSON.has("cloudcover")){
            cloud_cover = fJSON.getInt("cloudcover");}

         if (fJSON.has("chanceofrain")){
            chance_of_rain = fJSON.getInt("chanceofrain");}

         if (fJSON.has("chanceofwindy")){
            chance_of_windy = fJSON.getInt("chanceofwindy");}

         if (fJSON.has("chanceofsunny")){
            chance_of_sunny = fJSON.getInt("chanceosunny");}

         if (fJSON.has("chanceofsnow")){
            chance_of_snow = fJSON.getInt("chanceofsnow");}

         if (fJSON.has("chanceoffog")){
            chance_of_fog = fJSON.getInt("chanceoffog");}

         if (fJSON.has("weatherIconUrl")){
            weather_icon = fJSON.getJSONArray("weatherIconUrl").optJSONObject(0).getString("value");}
    }

    public Integer getDegrees() {
        return degrees;
    }

    public void setDegrees(Integer degrees) {
        this.degrees = degrees;
    }

    public Integer getWind_speedKmph() {
        return wind_speedKmph;
    }

    public void setWind_speedKmph(Integer wind_speedKmph) {
        this.wind_speedKmph = wind_speedKmph;
    }

    public Integer getWind_dirDegree() {
        return wind_dirDegree;
    }

    public void setWind_dirDegree(Integer wind_dirDegree) {
        this.wind_dirDegree = wind_dirDegree;
    }

    public Integer getPrecipMM() {
        return precipMM;
    }

    public void setPrecipMM(Integer precipMM) {
        this.precipMM = precipMM;
    }

    public Double getHumidity() {
        return humidity;
    }

    public void setHumidity(Double humidity) {
        this.humidity = humidity;
    }

    public Integer getVisibility() {
        return visibility;
    }

    public void setVisibility(Integer visibility) {
        this.visibility = visibility;
    }

    public Integer getPressure() {
        return pressure;
    }

    public void setPressure(Integer pressure) {
        this.pressure = pressure;
    }

    public Integer getCloud_cover() {
        return cloud_cover;
    }

    public void setCloud_cover(Integer cloud_cover) {
        this.cloud_cover = cloud_cover;
    }

    public Integer getChance_of_rain() {
        return chance_of_rain;
    }

    public void setChance_of_rain(Integer chance_of_rain) {
        this.chance_of_rain = chance_of_rain;
    }

    public Integer getChance_of_windy() {
        return chance_of_windy;
    }

    public void setChance_of_windy(Integer chance_of_windy) {
        this.chance_of_windy = chance_of_windy;
    }

    public Integer getChance_of_sunny() {
        return chance_of_sunny;
    }

    public void setChance_of_sunny(Integer chance_of_sunny) {
        this.chance_of_sunny = chance_of_sunny;
    }

    public Integer getChance_of_snow() {
        return chance_of_snow;
    }

    public void setChance_of_snow(Integer chance_of_snow) {
        this.chance_of_snow = chance_of_snow;
    }

    public Integer getChance_of_fog() {
        return chance_of_fog;
    }

    public void setChance_of_fog(Integer chance_of_fog) {
        this.chance_of_fog = chance_of_fog;
    }

    public Integer getChance_of_thunder() {
        return chance_of_thunder;
    }

    public void setChance_of_thunder(Integer chance_of_thunder) {
        this.chance_of_thunder = chance_of_thunder;
    }

    public String getWeather_icon() {
        return weather_icon;
    }

    public void setWeather_icon(String weather_icon) {
        this.weather_icon = weather_icon;
    }

    public String getWeather_background() {
        return weather_background;
    }

    public void setWeather_background(String weather_background) {
        this.weather_background = weather_background;
    }

}
