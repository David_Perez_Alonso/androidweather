package com.example.aimweatherapp;

import android.app.Activity;
import android.app.ListActivity;
import android.app.SearchManager;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import com.example.aimweatherapp.Utils.DBHelper;
import com.example.aimweatherapp.Utils.HttpUtils;
import com.example.aimweatherapp.Utils.JsonHelper;
import com.example.aimweatherapp.model.location;
import com.example.aimweatherapp.model.locationList;

public class searchCity extends Activity {

    locationList locations = new locationList();

    public static interface OnSearchCompleted{
        void OnSearchCompleted(locationList loc);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.searchcity);

        // Get the intent, verify the action and get the query
        Button btnSearch = (Button) findViewById(R.id.btnSearch);
        //Intent intent = getIntent();
        //if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
         //   String query = intent.getStringExtra(SearchManager.QUERY);
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView txtSearch = (TextView) findViewById(R.id.txtSearch);
                doMySearch(txtSearch.getText().toString().replace(" ","+"));
            }
        });

        ListView tView = (ListView) findViewById(R.id.listCity);
        tView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView txtCity = (TextView) findViewById(R.id.txtCity);
                TextView txtCountry = (TextView) findViewById(R.id.txtCountry);
                TextView txtCounty = (TextView) findViewById(R.id.txtCounty);

                location loc = locations.get(position);
                //location loc = new location(0,"",txtCountry.getText().toString(), txtCounty.getText().toString(), txtCity.getText().toString(),0,0);


                CursorAsyncTask cAsync = new CursorAsyncTask();
                cAsync.execute(loc);
            }
        });
    }

    void doMySearch(String query)
    {
        OnSearchCompleted listener = new OnSearchCompleted() {
            @Override
            public void OnSearchCompleted(locationList loc) {
                locations = loc;
            }
        };
        JsonHelper helper = new JsonHelper(listener);
        helper.loadCities(query, this);
    }

    private class CursorAsyncTask extends AsyncTask<location, Void, Boolean> {
        @Override
        protected Boolean doInBackground(location... params) {
            DBHelper dbHelper = new DBHelper(searchCity.this);
            dbHelper.insert(params[0]);
            MainActivity.locations.add(params[0]);
            return true;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Boolean b) {
            super.onPostExecute(b);
            MainActivity.listener.onAddedNew();
            searchCity.this.finish();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }
    }


}
