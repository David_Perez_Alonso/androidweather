package com.example.aimweatherapp.Utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.aimweatherapp.model.location;
import com.example.aimweatherapp.sqlite.DBBasics;
import com.example.aimweatherapp.sqlite.DBContract;

/**
 * Created by david on 27/12/13.
 */
public class DBHelper {
    private DBBasics dbBasics;
    private SQLiteDatabase db;


    public DBHelper(Context ctx)
    {
        dbBasics = new DBBasics(ctx);
    }

    public long insert(location loc)
    {
        if (db == null || db.isReadOnly())
        {
            this.close();
            db = dbBasics.getWritableDatabase();
        }

        ContentValues values = new ContentValues();
        values.put(DBContract.DBColumns.COLUMN_NAME_CITY, loc.getCity());
        values.put(DBContract.DBColumns.COLUMN_NAME_COUNTRY, loc.getCountry());
        values.put(DBContract.DBColumns.COLUMN_NAME_COUNTY, loc.getCounty());
        values.put(DBContract.DBColumns.COLUMN_NAME_LATITUDE, loc.getLatitude());
        values.put(DBContract.DBColumns.COLUMN_NAME_LONGITUDE, loc.getLongitude());

        long nuevoID = db.insert(DBContract.DBColumns.TABLE_NAME,null,values);

        return nuevoID;
    }

    public Cursor selectAll() {
        if (db == null) {
            db = dbBasics.getReadableDatabase();
        }

        // Define las columnas que nos devolverá la query
        String[] projection =  {
                DBContract.DBColumns._ID,
                DBContract.DBColumns.COLUMN_NAME_CITY,
                DBContract.DBColumns.COLUMN_NAME_COUNTRY,
                DBContract.DBColumns.COLUMN_NAME_COUNTY,
                DBContract.DBColumns.COLUMN_NAME_LATITUDE,
                DBContract.DBColumns.COLUMN_NAME_LONGITUDE
        };

        // Definimos el orden en el que se devuelven los resultados
        String order = DBContract.DBColumns._ID + " ASC";

        Cursor cursor = db.query(
                DBContract.DBColumns.TABLE_NAME, // The table to query
                projection,                                // The columns to return
                null,                                      // The columns for the WHERE clause
                null,                                      // The values for the WHERE clause
                null,                                      // don't group the rows
                null,                                      // don't filter by row groups
                order);                                    // The sort order

        return cursor;
    }

    public Cursor select(long id) {
        if (db == null) {
            db = dbBasics.getReadableDatabase();
        }

        // Define las columnas que nos devolverá la query
        String[] projection =  {
                DBContract.DBColumns._ID,
                DBContract.DBColumns.COLUMN_NAME_CITY,
                DBContract.DBColumns.COLUMN_NAME_COUNTRY,
                DBContract.DBColumns.COLUMN_NAME_COUNTY,
                DBContract.DBColumns.COLUMN_NAME_LATITUDE,
                DBContract.DBColumns.COLUMN_NAME_LONGITUDE
        };

        // Definimos el orden en el que se devuelven los resultados
        String order = DBContract.DBColumns._ID + " ASC";

        String where = DBContract.DBColumns._ID + " = ?";
        String whereArgs[] = { ""+id };

        Cursor cursor = db.query(
                DBContract.DBColumns.TABLE_NAME, // The table to query
                projection,                                // The columns to return
                null,                                      // The columns for the WHERE clause
                null,                                      // The values for the WHERE clause
                null,                                      // don't group the rows
                null,                                      // don't filter by row groups
                order);                                    // The sort order

        return cursor;
    }

    public int delete(long rowId) {
        if (db == null || db.isReadOnly()) {
            this.close();
            db = dbBasics.getWritableDatabase();
        }

        // Representa la cláusula WHERE
        String selection = DBContract.DBColumns._ID + " = ?";
        String[] selectionArgs = new String[] { ""+rowId };

        int count = db.delete(DBContract.DBColumns.TABLE_NAME, selection, selectionArgs);
        return count;
    }

    public int deleteAll() {
        if (db == null || db.isReadOnly()) {
            this.close();
            db = dbBasics.getWritableDatabase();
        }

        int count = db.delete(DBContract.DBColumns.TABLE_NAME, null, null);
        return count;
    }

    public int update(location loc) {
        if (db == null || db.isReadOnly()) {
            this.close();
            db = dbBasics.getWritableDatabase();
        }

        // Valores que se van a actualizar
        ContentValues values = new ContentValues();
        values.put(DBContract.DBColumns.COLUMN_NAME_CITY, loc.getCity());
        values.put(DBContract.DBColumns.COLUMN_NAME_COUNTRY, loc.getCountry());
        values.put(DBContract.DBColumns.COLUMN_NAME_COUNTY, loc.getCounty());
        values.put(DBContract.DBColumns.COLUMN_NAME_LATITUDE, loc.getLatitude());
        values.put(DBContract.DBColumns.COLUMN_NAME_LONGITUDE, loc.getLongitude());

        // Representa la cláusula WHERE
        String selection = DBContract.DBColumns._ID + " = ?";
        String[] selectionArgs = new String[] { ""+loc.getId() };

        int count = db.update(
                DBContract.DBColumns.TABLE_NAME,
                values,
                selection,
                selectionArgs);

        return count;
    }


    public void close() {
        if (db != null) {
            db.close();
            db = null;
        }
    }

    @Override
    protected void finalize() throws Throwable {
        this.close();

        super.finalize();
    }
}
