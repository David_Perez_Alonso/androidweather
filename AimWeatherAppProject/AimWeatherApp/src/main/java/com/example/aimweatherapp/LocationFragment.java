package com.example.aimweatherapp;

import android.app.Fragment;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.aimweatherapp.Utils.DBHelper;
import com.example.aimweatherapp.Utils.JsonHelper;
import com.example.aimweatherapp.model.location;
import com.example.aimweatherapp.sqlite.DBBasics;

/**
 * Created by david on 29/12/13.
 */
public class LocationFragment extends android.support.v4.app.Fragment{

    public final static String LOCATION_EXTRA = "location";
    long id =0;

    private location mLocation;

    public LocationFragment(long id)
    {
        this.id = id;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.fragment_main, menu);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_main, container, false);

        mLocation = getArguments().getParcelable(LOCATION_EXTRA);

        TextView txtMainCity = (TextView) v.findViewById(R.id.txtMainCity);
        txtMainCity.setText(mLocation.getCity());

        ProgressBar mProgress = (ProgressBar) v.findViewById(R.id.progress);
        mProgress.setVisibility(View.VISIBLE);

        JsonHelper helper = new JsonHelper();
        helper.loadForecast(mLocation, v);

        this.setHasOptionsMenu(true);


        /*TextView txtMainCountry = (TextView) v.findViewById(R.id.txtMainCountry);
        txtMainCountry.setText(mLocation.getCountry());

        TextView txtMainCounty = (TextView) v.findViewById(R.id.txtMainCounty);
        txtMainCounty.setText(mLocation.getCounty());*/

        return v;
    }


}
