package com.example.aimweatherapp.Utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.support.v7.appcompat.R;

import com.example.aimweatherapp.model.forecast;

/**
 * Created by david on 2/01/14.
 */
public class iconsManagement {

    public static Bitmap WEATHER;



    public static Bitmap get(forecast f, Context ctx)
    {
        WEATHER = BitmapFactory.decodeResource(ctx.getResources(), com.example.aimweatherapp.R.drawable.sunny);

        if (f.getChance_of_rain() > 0)
            WEATHER = BitmapFactory.decodeResource(ctx.getResources(), com.example.aimweatherapp.R.drawable.rain);

        if (f.getChance_of_rain() > 0 && f.getPrecipMM() > 10)
            WEATHER = BitmapFactory.decodeResource(ctx.getResources(), com.example.aimweatherapp.R.drawable.heavyrain);

        if(f.getChance_of_rain() == 0 && f.getCloud_cover() > 0)
            WEATHER = BitmapFactory.decodeResource(ctx.getResources(), com.example.aimweatherapp.R.drawable.cloud);

        if(f.getChance_of_rain() == 0 && f.getCloud_cover() > 0 && f.getChance_of_sunny() > 20)
            WEATHER = BitmapFactory.decodeResource(ctx.getResources(), com.example.aimweatherapp.R.drawable.sunnycloud);

        if(f.getChance_of_rain() == 0 && f.getWind_speedKmph() > 20)
            WEATHER = BitmapFactory.decodeResource(ctx.getResources(), com.example.aimweatherapp.R.drawable.wind);

        if(f.getChance_of_rain() > 0 && f.getWind_speedKmph() > 20)
            WEATHER = BitmapFactory.decodeResource(ctx.getResources(), com.example.aimweatherapp.R.drawable.windandrain);

        if(f.getChance_of_thunder() > 10)
            WEATHER = BitmapFactory.decodeResource(ctx.getResources(), com.example.aimweatherapp.R.drawable.thunder);

        if(f.getChance_of_snow() > 0)
            WEATHER = BitmapFactory.decodeResource(ctx.getResources(), com.example.aimweatherapp.R.drawable.snow);

        if(f.getChance_of_snow() > 10)
            WEATHER = BitmapFactory.decodeResource(ctx.getResources(), com.example.aimweatherapp.R.drawable.heavysnow);

        return WEATHER;
    }

}
