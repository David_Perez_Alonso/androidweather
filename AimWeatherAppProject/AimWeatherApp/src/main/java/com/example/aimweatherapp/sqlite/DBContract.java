package com.example.aimweatherapp.sqlite;

import android.provider.BaseColumns;

/**
 * Created by david on 27/12/13.
 */
public class DBContract {
    public static final String DATABASE_NAME = "AIMWEATHER.db";

    public static abstract class DBColumns implements BaseColumns {
        public static final String TABLE_NAME = "locations";
        public static final String COLUMN_NAME_CITY = "city";
        public static final String COLUMN_NAME_COUNTRY = "country";
        public static final String COLUMN_NAME_COUNTY = "county";
        public static final String COLUMN_NAME_LATITUDE = "latitude";
        public static final String COLUMN_NAME_LONGITUDE= "longitude";
    }

    // Se previene instanciar esta clase
    private DBContract() {}
}

