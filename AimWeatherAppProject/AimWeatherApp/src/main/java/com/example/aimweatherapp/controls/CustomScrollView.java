package com.example.aimweatherapp.controls;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.ScrollView;

/**
 * Created by david on 2/01/14.
 */


public class CustomScrollView extends HorizontalScrollView {
    GestureDetector gestureDetector;
    OnTouchListener gestureListener;
    private float xDistance, yDistance, lastX, lastY;

    public CustomScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFadingEdgeLength(0);
        gestureDetector = new GestureDetector(context,new MyGestureDetector());
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                xDistance = yDistance = 0f;
                lastX = ev.getX();
                lastY = ev.getY();
                setSmoothScrollingEnabled(true);
                //ObjectAnimator animator=ObjectAnimator.ofInt(this, "scrollX",(int) (float) lastX, (int) (float) lastY );
                //animator.start();
                smoothScrollTo((int) (float) lastX, (int) (float) lastY);
                break;
            case MotionEvent.ACTION_MOVE:
                final float curX = ev.getX();
                final float curY = ev.getY();
                xDistance += Math.abs(curX - lastX);
                yDistance += Math.abs(curY - lastY);
                lastX = curX;
                lastY = curY;
                if(xDistance > yDistance)
                    return false;
            case MotionEvent.ACTION_UP:
                return true;
            case MotionEvent.AXIS_HSCROLL:
                return true;
            case MotionEvent.ACTION_SCROLL:
                return true;

        }
        return true;
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        /*if( gestureDetector.onTouchEvent(event))
            return true;*/

        return false;
        //return super.onInterceptTouchEvent(ev) && mGestureDetector.onTouchEvent(ev);
    }



    // Return false if we're scrolling in the x direction
    class YScrollDetector extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            if(Math.abs(distanceY) > Math.abs(distanceX)) {
                return true;
            }
            return false;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            return super.onFling(e1, e2, velocityX, velocityY);
        }
    }

    class MyGestureDetector extends GestureDetector.SimpleOnGestureListener  {
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float     velocityY) {
            return false;
        }


    }
}