package com.example.aimweatherapp;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.SearchView;
import android.widget.TextView;

import com.example.aimweatherapp.model.location;
import com.example.aimweatherapp.model.locationList;

import java.util.HashSet;

/**
 * Created by david on 17/12/13.
 */
public class LocationAdapter extends FragmentStatePagerAdapter {

    private Activity ctx;

    private locationList locations;

    public LocationAdapter(FragmentManager fm, locationList locations) {
        super(fm);
        this.locations = locations;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    @Override
    public Fragment getItem(int i) {
        LocationFragment fragment = new LocationFragment(i);
        Bundle fragmentData = new Bundle();

        location loc = locations.getLocations().get(i);
        fragmentData.putParcelable(LocationFragment.LOCATION_EXTRA, loc);
        fragment.setArguments(fragmentData);

        return fragment;
    }

    @Override
    public int getCount() {
        return locations.getLocations().size();
    }

}
